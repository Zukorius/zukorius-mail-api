<?php 
require 'vendor/autoload.php';
require 'Config.php';
use Mailgun\Mailgun;

try {
    if (!array_key_exists('to', $_POST)) {
        throw new Exception("key 'to' not in request");
    }

    if (!array_key_exists('subject', $_POST)) {
        throw new Exception("key 'subject' not in request");
    }

    if (!array_key_exists('body', $_POST)) {
        throw new Exception("key 'body' not in request");
    }
} catch (Exception $e) {
    http_response_code(400);
    print("invalid json. Unable to send email. Error: " . $e->getMessage());
    exit();
}


try {
    $from = 'rachel@rachelvwhite.com';
    $to = $_POST['to'];
    $subject = $_POST['subject'];
    $body = $_POST['body'];

    $mg = Mailgun::create(Config::mailgunKey());
    $mg->messages()->send('rachelvwhite.com', [
        'from'    => $from,
        'to'      => $to,
        'subject' => $subject,
        'text'    => $body
    ]);
    var_dump($_POST);
} catch (Exception $e) {
    http_response_code(500);
    print 'Error sending email: ' . $e->getMessage();
}

